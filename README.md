```scala
$cat src/main/scala/net/X.scala 
package net

import json.{Json => AndyGlowJson}
import com.github.andyglow.jsonschema.AsCirce._
import json.schema.Version._
import io.circe._

object X {
  case class Foo(name: String)

  val fooSchema: Json = AndyGlowJson.schema[Foo].asCirce(Draft04())
}

$sbt console
scala> net.X.fooSchema.spaces2
res2: String =
{
  "properties" : {
    "name" : {
      "type" : "string"
    }
  },
  "additionalProperties" : false,
  "$schema" : "http://json-schema.org/draft-04/schema#",
  "type" : "object",
  "required" : [
    "name"
  ]
}
```