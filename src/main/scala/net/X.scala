package net

import com.github.andyglow.json.ToValue
import json.{Schema, Json => AndyGlowJson}
import com.github.andyglow.jsonschema.AsCirce
import com.github.andyglow.jsonschema.AsCirce._
import io.circe.{Codec, Decoder, DecodingFailure, Encoder, Json, KeyDecoder, KeyEncoder}
import json.schema.Version._
import io.circe.generic.semiauto
import json.schema.{title, typeHint}
import io.circe.generic.extras.semiauto._
import io.circe.generic.extras.Configuration

@typeHint[String]
sealed trait Discriminator
object Discriminator {
  implicit val ke: KeyEncoder[Discriminator] = KeyEncoder.instance(_.toString)
  implicit val kd: KeyDecoder[Discriminator] = KeyDecoder.instance {
    case "B" => Some(x)
    case "C" => Some(y)
    case _ => None
  }

  @typeHint[String]
  case object x extends Discriminator {
    override def toString: String = "B"
  }
  @typeHint[String]
  case object y extends Discriminator {
    override def toString: String = "C"
  }

  implicit val asValue: ToValue[Discriminator] = ToValue mk {
    case `x` => "B"
    case `y` => "C"
  }


}

case class Something(a: String)

case class Foo(m: Map[Discriminator, List[Something]])
object Foo {
  val schema = AndyGlowJson.schema[Foo].asCirce(Draft04())
}




