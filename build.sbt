scalaVersion := "2.13.0"

libraryDependencies += "com.github.andyglow" %% "scala-jsonschema-core" % "0.7.8"
libraryDependencies += "com.github.andyglow" %% "scala-jsonschema-circe-json" % "0.7.8"
libraryDependencies += "io.circe" %% "circe-generic-extras" % "0.14.1"
